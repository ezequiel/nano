#!/bin/bash

IMAGE_TAG=deb-builder

test -d debian || \
	fail "No 'debian' directory. Please run from the root of the source tree"

case "$1" in
  run)
    docker run -it --rm \
      --net=host \
      -u $(id -u):$(id -g) \
      -v /etc/group:/etc/group:ro \
      -v /etc/passwd:/etc/passwd:ro \
      -v /home/zeta/.gnupg:/home/zeta/.gnupg:ro \
      -v $(pwd)/..:/usr/src/ \
      -w /usr/src/$(basename $(pwd)) \
      $IMAGE_TAG /bin/bash
    ;;
  run-root)
    docker run -it --rm \
      --net=host \
      --privileged \
      -v $(pwd)/..:/usr/src \
      -w /usr/src/$(basename $(pwd)) \
      $IMAGE_TAG /bin/bash
    ;;
  build)
    pushd debian
    docker build -t $IMAGE_TAG .
    popd
    ;;
  *)
    echo "Usage: $0 {run|build}"
    exit 1
esac

